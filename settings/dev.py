import os
import pymongo
ENV = "development"
PORT = 8000
DEBUG = True
TESTING = True
DB_LOGIN = os.environ.get("DB_LOGIN", None)
DB_PASS = os.environ.get("DB_PASS", None)
# SRV from mongoDB Atlas
# client = pymongo.MongoClient(
#     "mongodb+srv://{}:{}@kost-cwn1x.mongodb.net/test?retryWrites=true".format(
#         DB_LOGIN, DB_PASS), connect=False
# )
# Local mongoDB
client = pymongo.MongoClient('localhost', 27017)
