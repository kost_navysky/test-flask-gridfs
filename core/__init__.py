from flask import Flask
from flask_cors import CORS
from news.urls import news

app = Flask(__name__)

app.config.from_object("settings.dev")

app.register_blueprint(news)

CORS(app)
