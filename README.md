## Requirements to run: 
- local mongod / atlas server (change it in _settings.dev_)
- setup venv
- install python pip requirements
```python
pip install -r requirements.txt
```
----
## Start project:
```python
python3 run.py
```
## Endpoints:
### POST
`localhost:8000/news/`
<br>Request type: formdata<br>
_Key - Value - Description_<br>
- image - <file>.jpeg / <file>.png / <file>.jpg - image file, standart extensions
- title - String
- content - String
- author - String
- tags - String


### GET
`localhost:8000/news/`
returns list of news, search for `image_id` field value

### GET
`localhost:8000/news/<image_id>`
returns image preview from back-end by GridFS

`so this endpoint also will render an image in HTML page if you use it inside
src="localhost:8000/news/<image_id>" attribute
`
