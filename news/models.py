import datetime
import pymongo
from bson import ObjectId
from settings.dev import client
from gridfs import GridFS


class News:
    __db = client["news_db"]
    __fs = GridFS(__db)
    __col = __db["news_col"]

    def __init__(self, **kwargs):
        self.__id = str(kwargs.get("_id"))
        self.title = kwargs.get("title", None)
        self.content = kwargs.get("content", None)
        self.image_name = kwargs.get("image_id", None)
        self.image_type = kwargs.get("image_type", None)
        self.author = kwargs.get("author", None)
        self.tags = kwargs.get("tags", None)
        self.datetime_created = kwargs.get("datetime_created", None)

    def create(self):
        news = {
            "title": self.title,
            "content": self.content,
            "author": self.author,
            "tags": self.tags,
            "datetime_created": datetime.datetime.utcnow()
        }
        created_news = self.__col.insert_one(news)
        if created_news.inserted_id:
            return True, str(created_news.inserted_id)
        else:
            return False, False

    @classmethod
    def get_image_id_by_filename(cls, filename):
        return cls.__fs.find_one({"filename": filename})._id

    @classmethod
    def get_image_by_id(cls, image_id):
        return cls.__fs.get(file_id=image_id)

    @classmethod
    def upload_image(cls, image, filename):
        cls.__fs.put(
            image,
            filename=filename,
            contentType=image.content_type
        )

    @classmethod
    def get_news(cls):
        news_cursor = cls.__col.find()
        news_list = [cls(**user).to_representation for user in news_cursor]
        return news_list

    @classmethod
    def image_connect(cls, updates):
        img_con = cls.__col.update_one(
            {"_id": ObjectId(updates.get("id"))},
            {
                "$set": {
                    "image_id": updates.get("image_id"),
                    "image_type": updates.get("image_type")
                    }
            }
        )
        # test practice to handle what is going on after pymongo query
        if img_con.matched_count == 1 and img_con.modified_count == 1:
            return True, True
        elif img_con.modified_count == 1 and img_con.modified_count == 0:
            return True, False
        else:
            return False, False

    @property
    def to_representation(self):
        return {
            "id": self.__id,
            "title": self.title,
            "content": self.content,
            "author": self.author,
            "tags": self.tags,
            "datetime_created": self.datetime_created,
            "image_id": self.image_name,
            "image_type": self.image_type
        }
