from flask import make_response, jsonify


def filter_by_schema(request_data: dict, schema: tuple) -> dict:
    return {
        field: value for field, value in request_data.items()
        if field in schema
    }


def get_auth_token(request) -> bool:
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return False
    return auth_header.split(" ")[-1] if auth_header.split(" ")[-1] else False


def create_response(message: dict, status_code: int):
    return make_response(jsonify(message), status_code)
