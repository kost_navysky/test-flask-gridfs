from flask import request, make_response
from flask.views import View

from .utils import filter_by_schema, create_response
from .models import News
from .schema import news_create_schema, news_image_schema


class NewsAPIView(View):
    methods = ['POST', "GET"]

    def dispatch_request(self):
        if request.method == "POST":
            # validation of empty data not implemented
            data = filter_by_schema(request.form, news_create_schema)
            created, inserted_news_id = News(**data).create()
            if created and inserted_news_id:
                image = request.files.get("image")
                filename = "news_{}".format(inserted_news_id)
                News().upload_image(image, filename)
                updates = {
                    "id": inserted_news_id,
                    "image_id": "news_{}".format(inserted_news_id),
                    "image_type": image.content_type
                }
                matched, modified = News.image_connect(updates)
                # test all states not for production version
                if matched and modified:
                    return create_response({"message": "ok, ok"}, 200)
                elif matched and not modified:
                    return create_response({"message": "ok, not ok"}, 400)
                else:
                    return create_response({"message": "not ok, not ok"}, 400)
            else:
                return create_response({"message": "news not created"}, 400)
        if request.method == "GET":
            return create_response({"news": News().get_news()}, 200)


class NewsImageAPIView(View):
    methods = ["GET"]

    def dispatch_request(self, image_id):
        if request.method == "GET":
            if not image_id:
                return create_response({"message": "provide image_id"}, 400)
            else:
                current_image_id = News().get_image_id_by_filename(
                    image_id
                )
                if current_image_id:
                    # get image from GridFS by _id
                    news_image = News().get_image_by_id(current_image_id)
                    # create response with image binary data in body
                    # and provide content type of image in header
                    resp = make_response(news_image.read(), 200)
                    resp.headers["Content-Type"] = news_image.content_type
                    return resp
