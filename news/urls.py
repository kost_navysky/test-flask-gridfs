from flask import Blueprint
from .views import NewsAPIView, NewsImageAPIView

news = Blueprint("news", __name__, url_prefix="/news")

news.add_url_rule(
    "/", view_func=NewsAPIView.as_view("news"), strict_slashes=False
)
news.add_url_rule(
    "/<image_id>", view_func=NewsImageAPIView.as_view("news_image"),
    strict_slashes=False
)
